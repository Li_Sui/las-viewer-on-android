LAS VIEWER ON ANDROID (2013 Summer project)
=========================

libLAS installation 
---------
see readme. Currently, there is a compiled version within jni/libs/armeabi-v7a/

move armeabi-v7a under projectRoot/libs

About project
--------
* LIDAR is a remote sensing technology that measures distance by illuminating a target with a laser and analysing the reflected light. 
3D data can be collected which is used in many fields including agriculture, geometrics, archaeology, geology, geomorphology, seismology, forestry.
Those data can be represented by certain data formats in a coordinate manner. SPD and LAS format will be discussed. 
* The goal of this project is to create a simple proof of concept LIDAR point cloud viewer application for a mobile device.
In addition, this report will discuss three methods that can handle large dataset in an efficient way.
* Since Android is open source which is widely used for smart-phone platform in the worldwide, the project will focus on Android system and its relevant technologies such as OpenGL ES.
* OpenGL ES provides a rendering technique that allow android to display and manipulate 3D graphic, so viewer can use in build sensor to orientate the view. Furthermore, user should be able to use inbuilt GPS to determine user's current location.
An example of use case is using this viewer application to view the structure of vegetation such as a forest.

Las Viewer on line 
---------
[only support firefox](http://lidarview.com/)