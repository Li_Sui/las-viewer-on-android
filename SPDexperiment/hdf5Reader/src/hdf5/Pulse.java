package hdf5;

import ch.systemsx.cisd.hdf5.CompoundElement;

public class Pulse {
	@CompoundElement(memberName = "GPS_TIME")
	private int gps_time;
	
	@CompoundElement(memberName = "PULSE_ID")
	private int pulse_id;
	
	@CompoundElement(memberName = "X_ORIGIN")
	private float x_origin;
	
	@CompoundElement(memberName = "Y_ORIGIN")
	private float y_origin;
	
	@CompoundElement(memberName = "Z_ORIGIN")
	private float z_origin;
	
	@CompoundElement(memberName = "H_ORIGIN")
	private float h_origin;
	
	@CompoundElement(memberName = "X_IDX")
	private float x_index;
	
	@CompoundElement(memberName = "Y_IDX")
	private float y_index;
	
	@CompoundElement(memberName = "AZIMUTH")
	private float azimuth;
	
	@CompoundElement(memberName = "ZENITH")
	private float zenith;
	
	@CompoundElement(memberName = "NUMBER_OF_RETURNS")
	private int number_return;
	
	@CompoundElement(memberName = "NUMBER_OF_WAVEFORM_TRANSMITTED_BINS")
	private int number_waveform_transmitted;
	
	@CompoundElement(memberName = "NUMBER_OF_WAVEFORM_RECEIVED_BINS")
	private int number_waveform_received;
	
	@CompoundElement(memberName = "RANGE_TO_WAVEFORM_START")
	private int range_waveform_start;
	
	@CompoundElement(memberName = "AMPLITUDE_PULSE")
	private float amplitude_pluse;
	
	@CompoundElement(memberName = "WIDTH_PULSE")
	private float width_pulse;
	
	@CompoundElement(memberName = "USER_FIELD")
	private int user_field;
	
	@CompoundElement(memberName = "SOURCE_ID")
	private int source_id;
	
	@CompoundElement(memberName = "SCANLINE")
	private int scan_line;
	
	@CompoundElement(memberName = "SCANLINE_IDX")
	private int scan_line_index;
	
	@CompoundElement(memberName = "RECEIVE_WAVE_NOISE_THRES")
	private float receive_waveform_noise_threshold;
	
	@CompoundElement(memberName = "TRANS_WAVE_NOISE_THRES")
	private float transmitted_waveform_noise_threshold;
	
	@CompoundElement(memberName = "WAVELENGTH")
	private float wave_length;
	
	@CompoundElement(memberName = "RECEIVE_WAVE_GAIN")
	private float receive_wave_gain;
	
	@CompoundElement(memberName = "RECEIVE_WAVE_OFFSET")
	private float receive_wave_offset;
	
	@CompoundElement(memberName = "TRANS_WAVE_GAIN")
	private float transmitted_wave_gain;
	
	@CompoundElement(memberName = "TRANS_WAVE_OFFSET")
	private float trasmitted_wave_offset;
	
	@CompoundElement(memberName = "PTS_START_IDX")
	private int point_offset;
	
	@CompoundElement(memberName = "TRANSMITTED_START_IDX")
	private int tansmitted_start_index;
	
	@CompoundElement(memberName = "RECEIVED_START_IDX")
	private int received_start_index;

	public int getGps_time() {
		return gps_time;
	}

	public void setGps_time(int gps_time) {
		this.gps_time = gps_time;
	}

	public int getPulse_id() {
		return pulse_id;
	}

	public void setPulse_id(int pulse_id) {
		this.pulse_id = pulse_id;
	}

	public float getX_origin() {
		return x_origin;
	}

	public void setX_origin(float x_origin) {
		this.x_origin = x_origin;
	}

	public float getY_origin() {
		return y_origin;
	}

	public void setY_origin(float y_origin) {
		this.y_origin = y_origin;
	}

	public float getZ_origin() {
		return z_origin;
	}

	public void setZ_origin(float z_origin) {
		this.z_origin = z_origin;
	}

	public float getH_origin() {
		return h_origin;
	}

	public void setH_origin(float h_origin) {
		this.h_origin = h_origin;
	}

	public float getX_index() {
		return x_index;
	}

	public void setX_index(float x_index) {
		this.x_index = x_index;
	}

	public float getY_index() {
		return y_index;
	}

	public void setY_index(float y_index) {
		this.y_index = y_index;
	}

	public float getAzimuth() {
		return azimuth;
	}

	public void setAzimuth(float azimuth) {
		this.azimuth = azimuth;
	}

	public float getZenith() {
		return zenith;
	}

	public void setZenith(float zenith) {
		this.zenith = zenith;
	}

	public int getNumber_return() {
		return number_return;
	}

	public void setNumber_return(int number_return) {
		this.number_return = number_return;
	}

	public int getNumber_waveform_transmitted() {
		return number_waveform_transmitted;
	}

	public void setNumber_waveform_transmitted(int number_waveform_transmitted) {
		this.number_waveform_transmitted = number_waveform_transmitted;
	}

	public int getNumber_waveform_received() {
		return number_waveform_received;
	}

	public void setNumber_waveform_received(int number_waveform_received) {
		this.number_waveform_received = number_waveform_received;
	}

	public int getRange_waveform_start() {
		return range_waveform_start;
	}

	public void setRange_waveform_start(int range_waveform_start) {
		this.range_waveform_start = range_waveform_start;
	}

	public float getAmplitude_pluse() {
		return amplitude_pluse;
	}

	public void setAmplitude_pluse(float amplitude_pluse) {
		this.amplitude_pluse = amplitude_pluse;
	}

	public float getWidth_pulse() {
		return width_pulse;
	}

	public void setWidth_pulse(float width_pulse) {
		this.width_pulse = width_pulse;
	}

	public int getUser_field() {
		return user_field;
	}

	public void setUser_field(int user_field) {
		this.user_field = user_field;
	}

	public int getSource_id() {
		return source_id;
	}

	public void setSource_id(int source_id) {
		this.source_id = source_id;
	}

	public int getScan_line() {
		return scan_line;
	}

	public void setScan_line(int scan_line) {
		this.scan_line = scan_line;
	}

	public int getScan_line_index() {
		return scan_line_index;
	}

	public void setScan_line_index(int scan_line_index) {
		this.scan_line_index = scan_line_index;
	}

	public float getReceive_waveform_noise_threshold() {
		return receive_waveform_noise_threshold;
	}

	public void setReceive_waveform_noise_threshold(
			float receive_waveform_noise_threshold) {
		this.receive_waveform_noise_threshold = receive_waveform_noise_threshold;
	}

	public float getTransmitted_waveform_noise_threshold() {
		return transmitted_waveform_noise_threshold;
	}

	public void setTransmitted_waveform_noise_threshold(
			float transmitted_waveform_noise_threshold) {
		this.transmitted_waveform_noise_threshold = transmitted_waveform_noise_threshold;
	}

	public float getWave_length() {
		return wave_length;
	}

	public void setWave_length(float wave_length) {
		this.wave_length = wave_length;
	}

	public float getReceive_wave_gain() {
		return receive_wave_gain;
	}

	public void setReceive_wave_gain(float receive_wave_gain) {
		this.receive_wave_gain = receive_wave_gain;
	}

	public float getReceive_wave_offset() {
		return receive_wave_offset;
	}

	public void setReceive_wave_offset(float receive_wave_offset) {
		this.receive_wave_offset = receive_wave_offset;
	}

	public float getTransmitted_wave_gain() {
		return transmitted_wave_gain;
	}

	public void setTransmitted_wave_gain(float transmitted_wave_gain) {
		this.transmitted_wave_gain = transmitted_wave_gain;
	}

	public float getTrasmitted_wave_offset() {
		return trasmitted_wave_offset;
	}

	public void setTrasmitted_wave_offset(float trasmitted_wave_offset) {
		this.trasmitted_wave_offset = trasmitted_wave_offset;
	}

	public int getPoint_offset() {
		return point_offset;
	}

	public void setPoint_offset(int point_offset) {
		this.point_offset = point_offset;
	}

	public int getTansmitted_start_index() {
		return tansmitted_start_index;
	}

	public void setTansmitted_start_index(int tansmitted_start_index) {
		this.tansmitted_start_index = tansmitted_start_index;
	}

	public int getReceived_start_index() {
		return received_start_index;
	}

	public void setReceived_start_index(int received_start_index) {
		this.received_start_index = received_start_index;
	}
	
	
	
	
	
}
