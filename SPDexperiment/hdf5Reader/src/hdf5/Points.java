package hdf5;

import ch.systemsx.cisd.hdf5.CompoundElement;

public class Points {
	
	@CompoundElement(memberName = "RETURN_ID")
	private int RETURN_ID;
	
	@CompoundElement(memberName = "GPS_TIME")
	private double GPS_TIME;
	
	@CompoundElement(memberName = "X")
	private float x;
	
	@CompoundElement(memberName = "Y")
	private float y;
	
	@CompoundElement(memberName = "Z")
	private float z;
	
	@CompoundElement(memberName = "HEIGHT")
	private float height;
	
	@CompoundElement(memberName = "RANGE")
	private float range;
	
	@CompoundElement(memberName = "AMPLITUDE_RETURN")
	private float amplitude_return;
	
	@CompoundElement(memberName = "WIDTH_RETURN")
	private float width_return;
	
	@CompoundElement(memberName = "RED")
	private int red;
	
	@CompoundElement(memberName = "GREEN")
	private int green;
	
	@CompoundElement(memberName = "BLUE")
	private int bule;
	
	@CompoundElement(memberName = "CLASSIFICATION")
	private int classification;
	
	@CompoundElement(memberName = "USER_FIELD")
	private int user_field;
	
	@CompoundElement(memberName = "IGNORE")
	private int ignore;
	
	@CompoundElement(memberName = "WAVE_PACKET_DESC_IDX")
	private int wave_packet_desc_idx;
	
	@CompoundElement(memberName = "WAVEFORM_OFFSET")
	private int waveform_offset;

	public int getRETURN_ID() {
		return RETURN_ID;
	}

	public void setRETURN_ID(int rETURN_ID) {
		RETURN_ID = rETURN_ID;
	}

	public double getGPS_TIME() {
		return GPS_TIME;
	}

	public void setGPS_TIME(double gPS_TIME) {
		GPS_TIME = gPS_TIME;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getRange() {
		return range;
	}

	public void setRange(float range) {
		this.range = range;
	}

	public float getAmplitude_return() {
		return amplitude_return;
	}

	public void setAmplitude_return(float amplitude_return) {
		this.amplitude_return = amplitude_return;
	}

	public float getWidth_return() {
		return width_return;
	}

	public void setWidth_return(float width_return) {
		this.width_return = width_return;
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public int getBule() {
		return bule;
	}

	public void setBule(int bule) {
		this.bule = bule;
	}

	public int getClassification() {
		return classification;
	}

	public void setClassification(int classification) {
		this.classification = classification;
	}

	public int getUser_field() {
		return user_field;
	}

	public void setUser_field(int user_field) {
		this.user_field = user_field;
	}

	public int getIgnore() {
		return ignore;
	}

	public void setIgnore(int ignore) {
		this.ignore = ignore;
	}

	public int getWave_packet_desc_idx() {
		return wave_packet_desc_idx;
	}

	public void setWave_packet_desc_idx(int wave_packet_desc_idx) {
		this.wave_packet_desc_idx = wave_packet_desc_idx;
	}

	public int getWaveform_offset() {
		return waveform_offset;
	}

	public void setWaveform_offset(int waveform_offset) {
		this.waveform_offset = waveform_offset;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}
	

}
