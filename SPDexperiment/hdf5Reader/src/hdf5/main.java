package hdf5;

import java.util.List;

import ch.systemsx.cisd.hdf5.HDF5DataSetInformation;
import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.HDF5LinkInformation;
import ch.systemsx.cisd.hdf5.IHDF5Reader;

public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		IHDF5Reader reader = HDF5Factory.openForReading("spd/LI080217_RAW9_10m_pmfgrd_h.spd");
		 
	        browse(reader, reader.object().getGroupMemberInformation("/", true), "");
	        reader.close();
	}
	
	  static void browse(IHDF5Reader reader, List<HDF5LinkInformation> members, String prefix)
	    {
	        for (HDF5LinkInformation info : members)
	        {
	            System.out.println(prefix + info.getPath() + ":" + info.getType());
	            switch (info.getType())
	            {
	                case DATASET:
	                    HDF5DataSetInformation dsInfo = reader.object().getDataSetInformation(info.getPath());
	                    System.out.println(prefix + "     " + dsInfo);
	                    break;
	                case SOFT_LINK:
	                    System.out.println(prefix + "     -> " + info.tryGetSymbolicLinkTarget());
	                    break;
	                case GROUP:
	                    browse(reader, reader.object().getGroupMemberInformation(info.getPath(), true),
	                            prefix + "  ");
	                    break;
	                default:
	                    break;
	            }
	        }
	    }

}
