package hdf5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import ch.systemsx.cisd.hdf5.HDF5DataBlock;
import ch.systemsx.cisd.hdf5.IHDF5Reader;

public class PointIter implements Iterator<List<Points>>  {
	
	private Iterator<HDF5DataBlock<Points[]>> iter;
	
	private final String pointSetPath="DATA/POINTS";
	
	public PointIter( final IHDF5Reader reader ){
		
		this.iter=reader.compound().getArrayBlocks( pointSetPath , Points.class).iterator();
	}
	@Override
	public boolean hasNext() {
		return iter.hasNext();
	}

	@Override
	public List<Points> next() {
		List<Points> listPoints=new ArrayList<Points>();
		Points[] points=iter.next().getData();
		listPoints.addAll( Arrays.asList( points ));
		return listPoints;
	}

	@Override
	public void remove() {
		//nothing
		
	}

}
