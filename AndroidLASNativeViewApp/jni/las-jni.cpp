
#include <string.h>
#include <jni.h>
#include <liblas/liblas.hpp>
#include <fstream>  
#include <iostream> 
#include <cstddef>

using liblas::Reader;
using liblas::Header;
using liblas::ReaderFactory;
using liblas::Point;
using std::string;

extern "C" {

JNIEXPORT jfloatArray JNICALL
	Java_main_java_nz_co_landcare_Native_Activity_MainActivity_pointsArrayNative(JNIEnv *env, jobject obj,jstring file){

		const char *str= env->GetStringUTFChars(file,0);

		ReaderFactory f;
		std::ifstream myfile( str, std::ios::in | std::ios::binary);
		Reader reader= f.CreateWithStream( myfile );
		Header const& header = reader.GetHeader();

	    int size=(header.GetPointRecordsCount()*3)+6;
	    jfloatArray jArray = env->NewFloatArray(size);
	    if (jArray != NULL) {

	    	if (float* ptr = env->GetFloatArrayElements(jArray, NULL)){

	    		ptr[0]=(float)header.GetMaxX();
	    		ptr[1]=(float)header.GetMinX();
	    		ptr[2]=(float)header.GetMaxY();
	    		ptr[3]=(float)header.GetMinY();
	    		ptr[4]=(float)header.GetMaxZ();
	    		ptr[5]=(float)header.GetMinZ();

	    		int i=6;
	    		while (reader.ReadNextPoint()){

	    				Point  const& p = reader.GetPoint();
	    				ptr[i] = (float)p.GetX();
	    				ptr[i+1]=(float)p.GetY();
	    				ptr[i+2]=(float)p.GetZ();
	    				i=i+3;

	    		}
	    		env->ReleaseFloatArrayElements(jArray, ptr, JNI_COMMIT);
	    	}

	     }

		 return jArray;
	}

}

