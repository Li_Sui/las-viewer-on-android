package main.java.nz.co.landcare.Native.View;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import main.java.nz.co.landcare.Native.Activity.MainActivity;


import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

/**
 * Render class
 * @author Li Sui
 *
 */
public class Render implements Renderer{
	
	private Points points;
	
	/**array of all coordinates*/
	private float[] arrayPoints;
	/**Maximum X*/
	private float maxX;
	/**Minimum X*/
	private float minX;
	/**Maximum Y*/
	private float maxY;
	/**Minimum Y*/
	private float minY;
	/**Maximum Z*/
	private float maxZ;
	/**Minimum Z*/
	private float minZ;
	
	/**rotation angle*/
	private float rt;
	
	public Render( ){
		
		float[]allPoints=MainActivity.nativeArray;
		/**Initialize coordinate array */
		this.arrayPoints=new float[allPoints.length-6];
		
		/**obtain maximum and minimum values*/
		this.maxX=allPoints[0];
		this.minX=allPoints[1];
		this.maxY=allPoints[2];
		this.minY=allPoints[3];
		this.maxZ=allPoints[4];
		this.minZ=allPoints[5];
		
		/**get rid of maximum and minimum values*/
		int k=0;
		for( int i=6;i<allPoints.length;i++ ){
			this.arrayPoints[k]=allPoints[i];
			k++;
		}
	}
	
	@Override
	public void onDrawFrame( final GL10 gl ) {
		
		gl.glClear( GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT );
		gl.glLoadIdentity();
		
		/**Rotate The point On The Z axis */
		gl.glRotatef(rt, 0.0f, 0.0f, 1.0f);	
		
		int centerX=(int)((maxX-minX)/2+minX);
		int centerY=(int)((maxY-minY)/2+minY);
		gl.glTranslatef(-centerX, -centerY, viewOnZ());  //z >= znearclipping+maxz

		points.draw(gl);
		
		rt+=0.2f;
	}
	
	private float viewOnZ(){
		
		if( maxZ<0 ){
			
			return maxZ*2;
		}else{
			
			return -maxZ*2;
		}
		
	} 

	@Override
	public void onSurfaceChanged( final GL10 gl, final int width, final int height ) {
		
		gl.glViewport( 0, 0, width, height ); 	
		gl.glMatrixMode( GL10.GL_PROJECTION ); 	
		gl.glLoadIdentity(); 					
		

		GLU.gluPerspective( gl, 125.0f, (float)width / (float)height, maxZ+50f, maxZ*5 );

		gl.glMatrixMode( GL10.GL_MODELVIEW ); 
		gl.glLoadIdentity(); 					
		
	}

	@Override
	public void onSurfaceCreated( final GL10 gl, final EGLConfig config ) {
		
		gl.glShadeModel( GL10.GL_SMOOTH ); 
		
		/**Background color */
		gl.glClearColor( 0.0f, 0.0f, 0.0f, 0.0f ); 
		
		points = new Points( arrayPoints );
		
		gl.glClearDepthf( 1 );
		gl.glEnable( GL10.GL_DEPTH_TEST );
		gl.glDepthFunc( GL10.GL_LEQUAL);
		gl.glHint( GL10.GL_POINT_SMOOTH_HINT, GL10.GL_NICEST ); 
	}

}
