package main.java.nz.co.landcare.Native.Activity;

import main.java.nz.co.landcare.Native.Fragment.OpenFileDialogFragment;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Bundle;
import android.view.Menu;

/**
 * MainActivity
 * @author Li Sui
 *
 */
public class MainActivity extends Activity  implements OpenFileDialogFragment.OpenFileDialogListener{
	
	public static float[] nativeArray;

	@Override
    public void onCreate( final Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        
        setContentView( R.layout.activity_main );
        
        Button chooseFile =( Button )findViewById ( R.id.openfile );
        chooseFile.setOnClickListener( new OnClickListener() {
        	
			@Override
			public void onClick( final View view ) {
				
				new OpenFileDialogFragment().show( getFragmentManager(), "dialog" );
			}
        });
    }
	
	/**Load the las wrapper library*/
	static {
	
	 System.loadLibrary( "las-jni" );
	}
	
	/**Native method: retrieve all points*/
	private native float[] pointsArrayNative( final String path );
	
	
	/**openfile dialog call back*/
	@Override
	public void onOpenFileClick( final String path, final String fileName ) {
		
		Intent myIntent = new Intent( this, ViewActivity.class );
		myIntent.putExtra( "fileName", fileName );
		MainActivity.nativeArray=pointsArrayNative( path );
		this.startActivity( myIntent );
	}
	 
	/**inflate the menu*/
	@Override
	public boolean onCreateOptionsMenu( final Menu menu ) {

		getMenuInflater().inflate( R.menu.main, menu );
		
		return true;
	}
}
