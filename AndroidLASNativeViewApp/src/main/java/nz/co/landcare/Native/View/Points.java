package main.java.nz.co.landcare.Native.View;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Points {
	
	private float[] vertices;
	
	private FloatBuffer vertexBuffer;
	
	public Points( final float[] pointsArray ){
		
		this.vertices=pointsArray;
		
		ByteBuffer byteBuf = ByteBuffer.allocateDirect( vertices.length *4 );
		byteBuf.order( ByteOrder.nativeOrder() );
		vertexBuffer = byteBuf.asFloatBuffer();
		vertexBuffer.put( vertices );
		vertexBuffer.position( 0 );
	}
	
	
	public void draw( final GL10 gl ) {		
		gl.glEnableClientState( GL10.GL_VERTEX_ARRAY );
		
		/**point size*/
	    gl.glPointSize(1);
	
	    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);

	    gl.glDrawArrays(GL10.GL_POINTS, 0, vertices.length/3);
	    gl.glDisableClientState( GL10.GL_VERTEX_ARRAY );

	}

}
