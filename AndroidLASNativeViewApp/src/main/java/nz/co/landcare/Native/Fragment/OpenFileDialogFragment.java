package main.java.nz.co.landcare.Native.Fragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

/**
 * open file dialog fragment
 * @author Li Sui
 *
 */
public class OpenFileDialogFragment extends DialogFragment {
	
	private OpenFileDialogListener listener;
	
	private List<File> fileList =null;
	
	private File baseDir =(File) Environment.getExternalStorageDirectory();
	
	private String[] fileSelection;
	
	private String chosenFile;
	
	public interface OpenFileDialogListener{
		
		public void onOpenFileClick( final String path, final String fileName );
		
	}
	
	@Override
	public void onAttach( final Activity activity ) {

		super.onAttach( activity );

		if (activity instanceof OpenFileDialogListener ) {
			listener = ( OpenFileDialogListener ) activity;
		}

		else {
			throw new ClassCastException( activity.toString()
				+ " must implemenet OpenFileDialogFragment.OpenFileDialogListener" );
		}
	}
	
	@Override
	public Dialog onCreateDialog( final Bundle savedInstanceState ) {
		
		
		if(fileList == null ){
			fileList =generateFileList( baseDir );

		}
		
		if( fileList == null ){
			Toast.makeText( getActivity(), "can't find las file", Toast.LENGTH_SHORT ).show();
		}
		
		fileSelection =new String[fileList.size()];
		
		for( int i =0; i< fileSelection.length; i++ ){
			
			fileSelection[i]=( String ) fileList.get( i ).getName();
		}
		
		return new AlertDialog.Builder( getActivity() )
		.setTitle( "choose file" )
		.setItems( fileSelection,new DialogInterface.OnClickListener(){

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				
				try{
					
					chosenFile = ( String ) fileList.get( which ).getCanonicalPath();		
				}catch ( final IOException e ){
					
					//nothing
				}
				if( chosenFile != null ){
					listener.onOpenFileClick( chosenFile ,fileSelection[which]);
				}
			}
			
		})
		.setNegativeButton( android.R.string.cancel, null )
		.create();
	}
	
	private List<File> generateFileList ( final File baseDir ){
		
		List<File> files;
		if( baseDir.list() ==null ){
			
			files =new ArrayList<File>( 0 );
			
		} else{
			
			files =new ArrayList<File>( baseDir.list().length );
			
			for ( File file : baseDir.listFiles() ){
				
				if( file .isDirectory() ){
					files.addAll( generateFileList( file ) );
				}else{
					
					if( file.toString().endsWith( ".las" )){

							 files.add( file );
					}
				}
			}
		}
		
		return files;
	}

}
