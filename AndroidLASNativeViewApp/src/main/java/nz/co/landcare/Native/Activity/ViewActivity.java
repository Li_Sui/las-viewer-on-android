package main.java.nz.co.landcare.Native.Activity;


import main.java.nz.co.landcare.Native.View.MyView;
import main.java.nz.co.landcare.Native.View.Render;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * ViewActivity
 * @author Li Sui
 *
 */
public class ViewActivity extends Activity{
	
	
	/** The OpenGL View */
	private MyView myView;
	
	/**The OpenGl Render*/
	private Render render;
	
	@Override
    public void onCreate( final Bundle savedInstanceState ){
		
		super.onCreate( savedInstanceState );
		
		setContentView(R.layout.view_activity);
		Intent intent = getIntent();
		TextView text =( TextView )findViewById( R.id.file_name );
		text.setText( intent.getStringExtra( "fileName" ));
		
		FrameLayout frame=( FrameLayout )findViewById( R.id.opengl_view );
		myView = new MyView( this );
			
		render=new Render();
		myView.setRenderer( render );
		
		frame.addView( myView );
    }
	
	/**
	 * resume the view
	 */
	@Override
	protected void onResume() {
		
		super.onResume();
		myView.onResume();
	}

	/**
	 * pause the view
	 */
	@Override
	protected void onPause() {
		
		super.onPause();
		myView.onPause();
	}


}
