How to configure and compile libLAS using Android-cmake on linux.

prerequisites:

  1. boost 1.45.0 (Minimum 1.38 and must be cmake version)

  2. libLAS-1.6 with LASer(LAS) file format at least 1.0 (Maximum 1.2)

  3. cmake (Minimum 2.6.0)

  4. android-cmake (note: You can use boost library they provided)

  5. android ndk r8b 

  6. android standalone tool chain (working best with arm/x86/mpi 4.4.3)


installation:

First, download android ndk and android-cmake (https://code.google.com/p/android-cmake/). Extract ndk and enter commmand

  $NDK/build/tools/make-standalone-toolchain.sh --platform=android-5 --install-dir=/where/you/want/to 

  You can specify which cup architecture you want to build with. For example:

  --toolchain=arm-linux-androideabi-4.4.3

  Here are some general targets
    * arm-linux-androideabi-4.4.3   => targetting ARM-based Android devices
    * x86-4.4.3                     => targetting x86-based Android devices
    * mipsel-linux-android-4.4.3    => targetting MIPS-based Android devices

Now,you should have a standalone toolchain. Next, Configure environment variables.

  standalone toolchain root:
     export ANDROID_STANDALONE_TOOLCHAIN=~/toolchain  

  android-cmake file:
     export ANDTOOLCHAIN=~/android-cmake/toolchain/android.toolchain.cmake

  alias trick:
    alias android-cmake='cmake -DCMAKE_TOOLCHAIN_FILE=$ANDTOOLCHAIN '


Now, it is the time to compile boost and libLAS libraries.

  boost:
       cd ~/android-cmake/common-libs/boost

       Pull down and patch the boost libraries for android.
       sh ./get_boost.sh
       mkdir build
       cd build
       android-cmake ..
       make
       sudo make install

  libLAS:

       cd ~/libLAS-1.6
       mkdir build
       cd build
       android-cmake ..
       make
       sudo make install


note: android-cmake currently only support ndk r8b -r8 -r7c -r7b -r7 -r6b -r6 -r5c -r5b -r5




