package nz.co.landcare.lasReader;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import nz.co.landcare.module.Header;
import nz.co.landcare.module.LASBlock;

/**
 *  LASReader class, read the header and point 
 * @author Li Sui
 *
 */
public class LASReader implements Iterator< LASBlock > {
	
	/**
	 * Access to binary file
	 */
	private  DataInputStream r;
	
	/**
	 * Init header
	 */
	private Header header;
	
	/**
	 * Convertion Factory
	 */
	private ConvertionFactory factory;
	
	/**
	 * count each point
	 */
	private int counter=0;
	
	int remainder;
	
	int blockID=0;
	
	int numberPointsPerBlock=100000;
	
	public LASReader( final InputStream in) throws IOException {
		
		r=new DataInputStream( in );
		
		factory=new ConvertionFactory();
		
		header=new Header();
		
		/**
		 * put information into header
		 */
		r.skipBytes(94);
		
		/**header size in bytes*/
		header.setHeaderSize( factory.getShort( r.readShort() ));
		
		/**Offset to point data*/
		header.setPointOffset( factory.getInt( r.readInt() ));
		
		r.skipBytes(4);
		/**Point Data Format ID(0,1,2,3)*/
		header.setFormatID( r.readUnsignedByte());
		
		r.skipBytes(2);
		/**Number of point records*/
		header.setNumberPoints( factory.getInt( r.readInt() ));
	
		r.skipBytes(20);
		/**point scale*/
		header.setxScale( factory.getDouble( r.readDouble() ));
		header.setyScale( factory.getDouble( r.readDouble() ));
		header.setzScale( factory.getDouble( r.readDouble() ));
		/**point offset*/
		header.setxOffset( factory.getDouble( r.readDouble() ));
		header.setyOffset( factory.getDouble( r.readDouble() ));
		header.setzOffset( factory.getDouble( r.readDouble() ));
		/**Max&Min points*/
		header.setMaxX( factory.getDouble( r.readDouble() ));
		header.setMinX( factory.getDouble( r.readDouble() ));
		header.setMaxY( factory.getDouble( r.readDouble() ));
		header.setMinY( factory.getDouble( r.readDouble() ));
		header.setMaxZ( factory.getDouble(r.readDouble() ));
		header.setMinZ( factory.getDouble( r.readDouble() ));
		
		/**skip to where point data stored*/
		r.skipBytes( header.getPointOffset()-header.getHeaderSize() );
		
		this.remainder=header.getNumberPoints()%numberPointsPerBlock;
	}
	
	/**
	 * get Header 
	 * @return Header
	 * @throws IOException
	 */
	public Header getHeader() throws IOException {
		
		return this.header;
	}

	@Override
	public boolean hasNext() {
		boolean onGoing = counter<header.getNumberPoints();
		if(onGoing==false){
			try {
				r.close();
			} catch ( final IOException e ) {
				e.printStackTrace();
			}
		}
		return onGoing;
	}


	@Override
	public LASBlock next() {
		
		LASBlock block =null;
		
		
		if(header.getFormatID()==0){
			block= doFormat0(block);
		}
		if(header.getFormatID()==1){
			block= doFormat1(block);
		}
		
		if(header.getFormatID()==2){
			block=doFormat2(block);
		}
		
		blockID++;
		return block;
	}


	@Override
	public void remove() {
		// Do nothing
		
	}
	
	private LASBlock doFormat0( LASBlock block ){
		
		int pointer=0;
		int skipByte=8;
		
		try {
			float[] listPoints;
			if( counter<(header.getNumberPoints()-remainder)){
				
				listPoints=new float[numberPointsPerBlock*3];
				
				/**read 500 points */
				for(int i=0;i<numberPointsPerBlock;i++){
					
					addPoints( skipByte,listPoints,pointer );
					pointer=pointer+3;
					counter++;
				}
				
			}else{
				
				listPoints=new float[remainder*3];
				
				for(int i=0;i<remainder;i++){
					
					addPoints( skipByte,listPoints,pointer );
					pointer=pointer+3;
					counter++;
				}
			}
			
			block=new LASBlock( blockID,listPoints); 
		} catch (final IOException e) {
			
			e.printStackTrace();
		}
		return block;
	}
	
	private LASBlock doFormat1( LASBlock block ){
		
		int pointer=0;
		int skipByte=16;
		try {
			float[] listPoints;
			if( counter<(header.getNumberPoints()-remainder)){
				listPoints=new float[numberPointsPerBlock*3];
				/**read 500 points */
				for(int i=0;i<numberPointsPerBlock;i++){
					
					addPoints( skipByte,listPoints,pointer);
					pointer=pointer+3;
					counter++;
				}
				
			}else{
				listPoints=new float[remainder*3];
				/**read remain points*/
				for(int i=0;i<remainder;i++){
					
					addPoints( skipByte,listPoints,pointer );
					pointer=pointer+3;
					counter++;
				}
			}
			
			block=new LASBlock( blockID,listPoints );
		} catch (final IOException e) {
			
			e.printStackTrace();
		}
		return block;
	}
	
	
private LASBlock doFormat2( LASBlock block ){
		
		int pointer=0;
		int skipByte=14;
		
		try {
			float[] listPoints;
			if( counter<(header.getNumberPoints()-remainder)){
				
				listPoints=new float[numberPointsPerBlock*3];
				
				/**read 500 points */
				for(int i=0;i<numberPointsPerBlock;i++){
					
					addPoints( skipByte,listPoints,pointer );
					pointer=pointer+3;
					counter++;
				}
				
			}else{
				
				listPoints=new float[remainder*3];
				
				for(int i=0;i<remainder;i++){
					
					addPoints( skipByte,listPoints,pointer );
					pointer=pointer+3;
					counter++;
				}
			}
			
			block=new LASBlock( blockID,listPoints);
		} catch (final IOException e) {
			
			e.printStackTrace();
		}
		return block;
	}
	
	
	private void addPoints(int skipByte ,float[] listPoints, int pointer) throws IOException{
		
		double x=( factory.getInt( r.readInt()) ) * header.getxScale() + header.getxOffset();
		double y=( factory.getInt( r.readInt()) ) * header.getyScale() + header.getyOffset();
		double z=( factory.getInt( r.readInt()) ) * header.getzScale() + header.getzOffset();
		
		
		listPoints[pointer]=(float) x;
		listPoints[pointer+1]=(float) y;
		listPoints[pointer+2]=(float) z;
		
		r.skipBytes( skipByte );
		
	}

}
