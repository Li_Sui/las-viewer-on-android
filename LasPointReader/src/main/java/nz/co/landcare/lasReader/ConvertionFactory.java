package nz.co.landcare.lasReader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Convert binary to Java 
 * 
 * @author Li Sui
 *
 */
public class ConvertionFactory {

	
	/**
	 * convert unsignedShort to int
	 * @param unsignedShort
	 * @return int
	 */
	protected int getShort( final Short unsignedShort ){
		
		ByteBuffer bb=ByteBuffer.allocateDirect(8);
		
		bb.putShort(unsignedShort).flip();
		
		int result =(int)(bb.order(ByteOrder.LITTLE_ENDIAN).getShort());
		bb.clear();
		
		return result;
	}
	
	/**
	 *  convert c double to java double
	 * @param double in C
	 * @return double
	 */
	protected  double getDouble( final double doubleInC ){
		
		ByteBuffer bb=ByteBuffer.allocateDirect(8);
		
		bb.putDouble(doubleInC).flip();
		
		double result =(double)(bb.order(ByteOrder.LITTLE_ENDIAN).getDouble());
		bb.clear();
		
		return result;
	}
	
	/**
	 * convert unsigned int to int
	 * @param unsignedInt
	 * @return int
	 */
	protected int getInt( final int unsignedInt ){
		
		ByteBuffer bb=ByteBuffer.allocateDirect(8);
		
		bb.putInt(unsignedInt).flip();
		
		int result =bb.order(ByteOrder.LITTLE_ENDIAN).getInt();
		bb.clear();
		
		return result;
	}
}
