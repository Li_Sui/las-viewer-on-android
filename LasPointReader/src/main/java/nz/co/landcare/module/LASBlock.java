package nz.co.landcare.module;


/**
 * 
 * @author Li Sui
 *
 */
public class LASBlock {
	
	private Integer ID;
	
	private float[] listPoints;
	
	public LASBlock( final Integer ID, final float[] listPoints ){
		
		this.ID=ID;
		this.listPoints=listPoints;

	}

	
	public Integer getID() {
		return ID;
	}

	public float[] getListPoints() {
		return listPoints;
	}
	
}
