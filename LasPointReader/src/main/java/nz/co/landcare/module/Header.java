package nz.co.landcare.module;

/**
 * Header module
 * @author Li Sui
 *
 */
public class Header {
	
	private int headerSize;
	
	private int pointOffset;
	
	private int formatID;
	
	private int numberPoints;
	
	private double xScale;
	private double yScale;
	private double zScale;
	
	private double xOffset;
	private double yOffset;
	private double zOffset;
	
	private double maxX;
	private double minX;
	private double maxY;
	private double minY;
	private double maxZ;
	private double minZ;
	
	public Header(){
		//Do nothing
	}

	public int getHeaderSize() {
		return headerSize;
	}

	public void setHeaderSize( final int headerSize ) {
		this.headerSize = headerSize;
	}

	public int getPointOffset() {
		return pointOffset;
	}

	public void setPointOffset( final int pointOffset ) {
		this.pointOffset = pointOffset;
	}

	public int getNumberPoints() {
		return numberPoints;
	}

	public void setNumberPoints( final int numberPoints ) {
		this.numberPoints = numberPoints;
	}

	public double getxScale() {
		return xScale;
	}

	public void setxScale( final double xScale ) {
		this.xScale = xScale;
	}

	public double getyScale() {
		return yScale;
	}

	public void setyScale( final double yScale ) {
		this.yScale = yScale;
	}

	public double getzScale() {
		return zScale;
	}

	public void setzScale( final double zScale ) {
		this.zScale = zScale;
	}

	public double getxOffset() {
		return xOffset;
	}

	public void setxOffset( final double xOffset ) {
		this.xOffset = xOffset;
	}

	public double getyOffset() {
		return yOffset;
	}

	public void setyOffset( final double yOffset ) {
		this.yOffset = yOffset;
	}

	public double getzOffset() {
		return zOffset;
	}

	public void setzOffset( final double zOffset ) {
		this.zOffset = zOffset;
	}

	public double getMaxX() {
		return maxX;
	}

	public void setMaxX( final double maxX ) {
		this.maxX = maxX;
	}

	public double getMinX() {
		return minX;
	}

	public void setMinX( final double minX ) {
		this.minX = minX;
	}

	public double getMaxY() {
		return maxY;
	}

	public void setMaxY( final double maxY ) {
		this.maxY = maxY;
	}

	public double getMinY() {
		return minY;
	}

	public void setMinY( final double minY ) {
		this.minY = minY;
	}

	public double getMaxZ() {
		return maxZ;
	}

	public void setMaxZ( final double maxZ ) {
		this.maxZ = maxZ;
	}

	public double getMinZ() {
		return minZ;
	}

	public void setMinZ( final double minZ ) {
		this.minZ = minZ;
	}

	public int getFormatID() {
		return formatID;
	}

	public void setFormatID(final int formatID) {
		this.formatID = formatID;
	}
	
}
