package nz.co.landcare.sampleDataTests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;

import nz.co.landcare.lasReader.LASReader;
import nz.co.landcare.module.LASBlock;

import org.junit.Before;
import org.junit.Test;

/**
 * Point tests
 * @author Li Sui
 *
 */
public class LASBlockTests {
	
	private LASBlock block;

	private LASReader las;

	@Before
	public void setUp() throws Exception {
		
		/**path to sample file*/
		las=new LASReader(new FileInputStream("src/test/resources/nz/co/landcare/sampleDataTests/lidar.las"));
	
		/**Test get first block*/
		block=las.next();
	}

	
	@Test
	public void testGetID(){
		
		assertTrue( block.getID() == 0);
	}
	
	@Test
	public void testGetListPoints(){
		
		assertNotNull( block.getListPoints() );
	}
}
