package nz.co.landcare.sampleDataTests;

import static org.junit.Assert.*;

import java.io.FileInputStream;

import nz.co.landcare.lasReader.LASReader;
import nz.co.landcare.module.Header;

import org.junit.Before;
import org.junit.Test;

/**
 * Header tests
 * @author Li Sui
 *
 */
public class HeaderTests {
	
	private Header header;

	@Before
	public void setUp() throws Exception {
		
		/**path to sample file*/
		LASReader las=new LASReader(new FileInputStream("src/test/resources/nz/co/landcare/sampleDataTests/lidar.las"));
		header=las.getHeader();
		
	}

	@Test
	public void testGetHeaderSize() {
		
		assertTrue( header.getHeaderSize() == 227 );
	}

	@Test
	public void testGetPointOffset() {
		
		assertTrue( header.getPointOffset() == 1220 );
	}

	@Test
	public void testGetNumberPoints() {
		
		assertTrue( header.getNumberPoints() == 10653 );
	}

	@Test
	public void testGetxScale() {
		
		assertTrue( (Math.round(header.getxScale() * 100.0 ) / 100.0) == 0.01 );
	}

	@Test
	public void testGetyScale() {
		
		assertTrue( (Math.round(header.getyScale() * 100.0 ) / 100.0) == 0.01 );
	}
	
	@Test
	public void testGetzScale() {
		
		assertTrue( (Math.round(header.getzScale() * 100.0 ) / 100.0) == 0.01 );
	}

	@Test
	public void testGetxOffset() {
		
		assertTrue( (Math.round(header.getxOffset() * 100.0 ) / 100.0) == -0.0 );
	}

	@Test
	public void testGetyOffset() {
		
		assertTrue( (Math.round(header.getyOffset() * 100.0 ) / 100.0) == -0.0 );
	}

	@Test
	public void testGetzOffset() {
		
		assertTrue( (Math.round(header.getzOffset() * 100.0 ) / 100.0) == -0.0 );
	}

	@Test
	public void testGetMaxX() {
		
		assertTrue( (Math.round(header.getMaxX() * 100.0 ) / 100.0) == 638994.75 );
	}

	@Test
	public void testGetMinX() {
		
		assertTrue( (Math.round(header.getMinX() * 100.0 ) / 100.0) == 635589.01 );
	}

	@Test
	public void testGetMaxY() {
		
		assertTrue( (Math.round(header.getMaxY() * 100.0 ) / 100.0) == 853535.43 );
	}

	@Test
	public void testGetMinY() {

		assertTrue( (Math.round( header.getMinY() * 100.0 ) / 100.0) == 848886.45 );
	}

	@Test
	public void testGetMaxZ() {
		
		assertTrue( (Math.round(header.getMaxZ() * 100.0 ) / 100.0) ==  593.73 );
	}

	@Test
	public void testGetMinZ() {
		
		assertTrue( (Math.round(header.getMinZ() * 100.0 ) / 100.0) ==  406.59 );
	}

	@Test
	public void testGetFormatID() {
		
		assertTrue( header.getFormatID() ==1 );
	}

}
