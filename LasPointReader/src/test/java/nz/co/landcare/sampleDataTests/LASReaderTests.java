package nz.co.landcare.sampleDataTests;

import static org.junit.Assert.*;

import java.io.FileInputStream;

import nz.co.landcare.lasReader.LASReader;

import org.junit.Before;
import org.junit.Test;

/**
 * LASReader tests
 * @author Li Sui
 *
 */
public class LASReaderTests {

	private LASReader las;
	
	@Before
	public void setUp() throws Exception {
		
		/**path to sample file*/
		las=new LASReader(new FileInputStream("src/test/resources/nz/co/landcare/sampleDataTests/lidar.las"));
	}

	@Test
	public void testHasNext() {
		
	    assertTrue(las.hasNext());
	}

	@Test
	public void testNext() {
		
		assertNotNull( las.next() );
	}

}
