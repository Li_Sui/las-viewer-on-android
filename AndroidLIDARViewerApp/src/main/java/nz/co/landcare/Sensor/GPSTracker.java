package main.java.nz.co.landcare.Sensor;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

/**
 * GPS Tracker
 * @author Li Sui
 *
 */
public class GPSTracker extends Service implements LocationListener {
 
    private final Context mContext;
 
    /**flag for GPS status*/
    boolean isGPSEnabled = false;
 
    /** flag for network status*/
    boolean isNetworkEnabled = false;
 
    /** flag for GPS status*/
    boolean canGetLocation = false;
 
    Location location; 
    double latitude; 
    double longitude; 
 
    /**The minimum distance to change Updates in meters*/
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = (long) 1 ;//  1 meters
    /**The minimum time between updates in milliseconds*/
    private static final long MIN_TIME_BW_UPDATES = 1000 ; 
 
    protected LocationManager locationManager;
    
    /**
     *  constructor
     * @param context
     */
    public GPSTracker( final Context context){
        this.mContext = context;
        getLocation();
    }
    
    /**
     * get location
     * @return currentLocation
     */
    public Location getLocation() {
    	
        try {
        	
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);
 
            /** getting GPS status*/
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
 
            /** getting network status*/
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
 
            if ( !isGPSEnabled && !isNetworkEnabled ) {
                /**no network provider is enabled*/
            } else {
            	
                this.canGetLocation = true;
                /**First get location from Network Provider*/
                
                if ( isNetworkEnabled ){
                	
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    
                    if ( locationManager != null ){
                    	
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        
                        if ( location != null ){
                        	
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                /** if GPS Enabled get lat/long using GPS Services*/
                if ( isGPSEnabled ){
                	
                    if ( location == null ){
                    	
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        if ( locationManager != null ){
                        	
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if ( location != null ){
                            	
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
 
        } catch ( final Exception e ) {
            e.printStackTrace();
        }

        return location;
    }
     
    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in app
     * */
    public void stopUsingGPS(){
    	
        if( locationManager != null ){
        	
            locationManager.removeUpdates( GPSTracker.this );
        }       
    }
     
    /**
     *  get latitude
     * 
     * */
    public double getLatitude(){
    	
        if( location != null ){
        	
            latitude = location.getLatitude();
        }
        return latitude;
    }
     
    /**
     * get longitude
     * 
     * */
    public double getLongitude(){
    	
        if( location != null ){
        	
            longitude = location.getLongitude();
        }

        return longitude;
    }
     
    /**
     *  check GPS/wifi enabled
     * 
     * @return boolean
     * */
    public boolean canGetLocation() {
    	
        return this.canGetLocation;
    }
     
    /**
     *  show settings alert dialog
     * On pressing Settings button will launch Settings Options
     * 
     * */
    public void showSettingsAlert(){
    	
        AlertDialog.Builder alertDialog = new AlertDialog.Builder( mContext );
      
        /**Setting Dialog Title*/
        alertDialog.setTitle( "GPS is settings" );
  
        /**Setting Dialog Message*/
        alertDialog.setMessage( "GPS is not enabled. Do you want to go to settings menu?" );
  
        /**On pressing Settings button*/
        alertDialog.setPositiveButton( "Settings", new DialogInterface.OnClickListener() {
        	
            public void onClick( final DialogInterface dialog,final int which ){
            	 
                Intent intent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                mContext.startActivity( intent );
            }
        });
  
        /**on pressing cancel button*/
        alertDialog.setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
        	
            public void onClick( final DialogInterface dialog, final int which ){
            	
            	dialog.cancel();
            }
        });
        
        alertDialog.show();
    }
 
    @Override
    public void onLocationChanged( final Location location ){
    	//not in use
    }
 
    @Override
    public void onProviderDisabled( final String provider ){
    	//not in use
    }
 
    @Override
    public void onProviderEnabled( final String provider ){
    	//not in use
    }
 
    @Override
    public void onStatusChanged( final String provider, final int status, final Bundle extras ){
    	//not in use
    }
 
    @Override
    public IBinder onBind( final Intent arg0 ){
    	//not in use
        return null;
    }
 
}
