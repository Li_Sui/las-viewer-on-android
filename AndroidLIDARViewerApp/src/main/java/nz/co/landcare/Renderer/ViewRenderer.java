package main.java.nz.co.landcare.Renderer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import main.java.nz.co.landcare.Activity.ViewActivity;
import nz.co.landcare.module.Header;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

/**
 * Render class
 * @author Li Sui
 *
 */
public class ViewRenderer implements Renderer{
	
	private GL11 gl11;
	/**Maximum X*/
	private float maxX;
	/**Minimum X*/
	private float minX;
	/**Maximum Y*/
	private float maxY;
	/**Minimum Y*/
	private float minY;
	/**Maximum Z*/
	private float maxZ;
	/**Minimum Z*/
	//private float minZ;
	
	private Header header;
	
	private float[] points;
	
	public float yaw=0f;
	public float pitch=0f;
	public float roll=0f;
	public float changeY=0f;
	
	public boolean overviewFlag = true;
	
	private float animateRotation=0f;

	private float selfRotation=0f;
	private float currentX=0f;
	private float currentY=0f;
	private float currentZ=0f;
	
	public float[] getPoints(){
		return points;
	}

	public ViewRenderer( ) throws IOException{
			
		header = ViewActivity.header;
		points=new float[header.getNumberPoints()*3];

		this.maxX=(float) header.getMaxX();
		this.minX=(float) header.getMinX();
			
		this.maxY=(float) header.getMaxY();
		this.minY=(float) header.getMinY();
			
		this.maxZ=(float) header.getMaxZ();
		//this.minZ=(float)header.getMinZ();
		
		currentX=viewOnX();
		currentY=viewOnY();
		currentZ=viewOnZ();
	}
	/**
	 * will be called on drawing each frame
	 */
	@Override
	public void onDrawFrame( final GL10 gl ) {
		
		/**clear Screen And Depth Buffer*/
		gl11.glClear( GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT );
		gl11.glLoadIdentity();
		
		if(overviewFlag){
			drawOverviewScene();
		}else{
			drawDetailScene();
		}
		draw(gl11);
	}
	/**
	 * 
	 * Calculate middle point x
	 */
	private float viewOnX(){
		if(maxX<0){
			return ((maxX-minX)/2+minX);
		}else{
			return -((maxX-minX)/2+minX);
		}
	}
	/**
	 * 
	 * Calculate middle point y
	 */
	private float viewOnY(){
		if(maxY<0){
			return ((maxY-minY)/2+minY);
		}else{
			return -((maxY-minY)/2+minY);
		}
	}
	
	/**
	 * 
	 * Calculate the distance between eye and object
	 */
	private float viewOnZ(){
		
		float distanceToX = (float) ((maxX-minX)/(2*Math.tan(125/2)));;
		float distanceToY = (float) ((maxY-minY)/(2*Math.tan(125/2)));

		return (float) (Math.max(distanceToX, distanceToY));	
	} 
	
	@Override
	public void onSurfaceChanged( final GL10 gl, final int width, final int height ) {
		
		gl11.glViewport( 0, 0, width, height ); 	
		
		gl11.glMatrixMode( GL11.GL_PROJECTION ); 	
		gl11.glLoadIdentity(); 	
		/**perspective argument(glAPI.  
		 * view angle in degrees in the y direction. 
		 * Specifies the aspect ratio that determines the field of view in the x direction. The aspect ratio is the ratio of x (width) to y (height).
		 * Specifies the distance from the viewer to the near clipping plane (always positive).
		 * Specifies the distance from the viewer to the far clipping plane (always positive).)*/
		GLU.gluPerspective( gl11, 125.0f, (float)width / (float)height, 5f, Math.max(maxX, maxY)+100);	
		gl11.glMatrixMode( GL11.GL_MODELVIEW ); 
		/**Reset The Current Modelview Matrix*/
	}

	@Override
	public void onSurfaceCreated( final GL10 gl, final EGLConfig config ) {
		
		gl11=(GL11)gl;
		/**Background color */
		gl11.glClearColor( 0f, 0f, 0f, 1f ); 
		
		gl11.glClearDepthf( 1.0f );
		
		gl11.glDepthFunc( GL11.GL_LEQUAL);
		
//		gl11.glHint(GL11.GL_POINT_SMOOTH_HINT, GL11.GL_NICEST);
//	    gl11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
//	    gl11.glHint(GL11.GL_FOG_HINT, GL11.GL_NICEST);
//	    
		gl11.glShadeModel( GL11.GL_SMOOTH ); 
	}
	
	/**Draw the object*/
	private void draw( final GL11 gl ) {	
		
		ByteBuffer byteBuf = ByteBuffer.allocateDirect( points.length * 4 );
		byteBuf.order( ByteOrder.nativeOrder() );
		FloatBuffer buffer = byteBuf.asFloatBuffer();
		
		buffer.put( points );
		buffer.position( 0 );
		
		gl.glEnableClientState( GL11.GL_VERTEX_ARRAY );
		/**point size*/
		gl.glPointSize(1.0f);
		/**point color*/
		//gl.glColor4f (0f, 0f, 0f, 0f);
		gl.glVertexPointer(3, GL11.GL_FLOAT, 0, buffer );
	    gl.glDrawArrays(GL11.GL_POINTS, 0, header.getNumberPoints());
	    gl.glDisableClientState( GL11.GL_VERTEX_ARRAY );
	    
	    byteBuf.clear();
	    buffer.clear();
	}
	private void drawOverviewScene(){
		gl11.glRotatef(animateRotation, 1, 0, 0);
		gl11.glRotatef(selfRotation, 0, 0, 1);
		gl11.glTranslatef(currentX, currentY, currentZ);
		
		/**move eye from center back to overview position*/
		if(animateRotation <0){
			animateRotation=(float) (animateRotation+1.7);
		}
		if(currentZ > viewOnZ() ){
			currentZ=currentZ-40;
		}
		
		selfRotation+=0.2f;
	}
	private void drawDetailScene(){
		
		gl11.glRotatef(animateRotation , 1, 0, 0);
		/**must be in this order*/
		gl11.glRotatef(roll, 0, 1, 0);
		gl11.glRotatef(pitch, 1, 0, 0);
		gl11.glRotatef (yaw, 0, 0, 1);
		
		gl11.glTranslatef(currentX+changeY, currentY+changeY, currentZ);
		
		/**move eye from overview position to center */
		if(animateRotation >-90){
			animateRotation=(float) (animateRotation-1.7);
		}
		if(currentZ < -maxZ){
			currentZ=currentZ+20;
		}
	}
}
