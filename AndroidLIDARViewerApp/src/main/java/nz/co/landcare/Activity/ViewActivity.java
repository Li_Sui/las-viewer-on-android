package main.java.nz.co.landcare.Activity;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import main.java.nz.co.landcare.Renderer.ViewRenderer;
import main.java.nz.co.landcare.Sensor.GPSTracker;
import main.java.nz.co.landcare.Sensor.Orientation;
import nz.co.landcare.lasReader.LASReader;
import nz.co.landcare.module.Header;
import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * ViewActivity 
 * include overview and detailed view
 * 
 * @author Li Sui
 *
 */
public class ViewActivity extends Activity implements Orientation.Listener {
	
	
	/** The OpenGL View */	
	private GLSurfaceView view;
	/**The renderer*/
	private ViewRenderer viewRender;

	private Orientation mOrientation;

	
	/**touch factor*/
	//private float previousX;
	private float previousY;
	private float TOUCH_SCALE_FACTOR=180.0f/320.0f;
	
	/** access to header file*/
	public static Header header;
	/**las reader*/
	private LASReader las;
	
	/**indicate how many  points per block*/
	int numberPointsPerBlock=100000;
	
	/**rendering flag*/
	private boolean stopRendering=false;
	
	@Override
    public void onCreate( final Bundle savedInstanceState ){
		
		super.onCreate( savedInstanceState );
		setContentView(R.layout.overview_activity);
		
		mOrientation = new Orientation((SensorManager) getSystemService(Activity.SENSOR_SERVICE),getWindow().getWindowManager());
		
		Intent intent = getIntent();
		TextView text =( TextView )findViewById( R.id.file_name );
		FrameLayout frame=( FrameLayout )findViewById( R.id.opengl_overview );
		
		/**locate me button*/
		final Button locateButton=( Button )findViewById( R.id.locate_me);
		
		final Button backButton=( Button )findViewById( R.id.back);
		
		backButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick( final View myview) {
				
				locateButton.setVisibility( View.VISIBLE );
				viewRender.overviewFlag=true;
				backButton.setVisibility( View.INVISIBLE );
			}
			
		});
		backButton.setVisibility(View.INVISIBLE);
		/**locate me button click listener*/
		locateButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick( final View myview ) {
				
				/**init GPS Tracker*/
				GPSTracker gps = new GPSTracker( view.getContext() );
				
				/**check is user enable location service*/
				if( gps.canGetLocation() ){
                
	                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + (float) gps.getLatitude() + "\nLong: " + (float) gps.getLongitude(), Toast.LENGTH_LONG).show();
	                /**set overview mode to false*/
	                viewRender.overviewFlag=false;
	               
					/**hide the button*/
					locateButton.setVisibility( View.INVISIBLE );
					backButton.setVisibility( View.VISIBLE );
                
				}else{
					gps.showSettingsAlert();
				}
			}
		});
		/**show the file name*/
		text.setText( intent.getStringExtra( "fileName" ));
		/**init the view*/
		view=new GLSurfaceView(this);
		
		/** obtain all blocks and transfer into float array concurrently*/
		try {
			loadPoints(intent.getBooleanExtra("exampleFlag", true), intent.getStringExtra("fileName"),intent.getStringExtra("filePath"));

		} catch ( final IOException e ) {
			Toast.makeText( this, "can't load file", Toast.LENGTH_SHORT ).show();//exceptions
		}
		/**set the render*/
		view.setRenderer(viewRender);
		 view.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
		view.requestFocus();
		view.setFocusableInTouchMode(true);
		
		/**add the view*/
		frame.addView( view );
    }
	
	/**
	 * resume the view
	 */
	@Override
	protected void onResume() {
		
		super.onResume();
		view.onResume();
	    /** for the system's orientation sensor registered listeners*/
		mOrientation.startListening(this);
	}

	/**
	 * pause the view
	 */
	@Override
	protected void onPause() {
		
		super.onPause();
		view.onPause();
		/**stop the listener and save battery*/
		mOrientation.stopListening();
	    /**stop rendering*/
	    this.stopRendering=true;
	}
	
	/**
	 * on back button pressed event
	 */
	@Override
	public void  onBackPressed(){
		super.onBackPressed();
		/**stop rendering*/
		this.stopRendering=true;
	}
	
	/**
	 * on touch event 
	 */
	@Override
	public boolean onTouchEvent( final MotionEvent evt ){
		
		//float currentX =evt.getX();
		float currentY=evt.getY();
		float deltaY;
		
		switch( evt.getAction() ){
		
			case MotionEvent.ACTION_MOVE:
				//deltaX=currentX-previousX;
				deltaY=currentY-previousY;
				 viewRender.changeY+=deltaY*TOUCH_SCALE_FACTOR;
				//viewRender.changeX+=deltaX*TOUCH_SCALE_FACTOR;
		}  
		//previousX=currentX;
		previousY=currentY;
		return true;
	}

	@Override
	public void onOrientationChanged(float yaw ,float pitch, float roll) {
		viewRender.yaw=yaw;
		viewRender.roll=roll;
		viewRender.pitch=pitch;
	}
	
	/**
	 * obtain all blocks and transfer into float array concurrently
	 * @param exampleFlag
	 * @param fileName
	 * @param filePath
	 * @throws IOException
	 */
	private void loadPoints( final boolean exampleFlag, final String fileName, final String filePath ) throws IOException{
		if( exampleFlag ){
			las=new LASReader( this.getAssets().open(fileName) );
		}else{
			las=new LASReader( new FileInputStream( filePath) );
		}
		header=las.getHeader();
		viewRender = new ViewRenderer();
		
		final int maxNumber= header.getNumberPoints();
		final int numberOfBlock =(maxNumber-maxNumber%numberPointsPerBlock)/numberPointsPerBlock+1;
		Toast.makeText( this, String.valueOf( numberOfBlock )+" blocks in total", Toast.LENGTH_SHORT ).show();
		ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
		cachedThreadPool.execute(new Runnable() {

			@Override
			public void run() {
				float[] points = viewRender.getPoints();
				int i = 0;
				 while (las.hasNext() && !stopRendering ){
					 
					synchronized( points ) {
							for ( float point : las.next().getListPoints() ) {
							points[ i++ ] = point;
						}
					};
				}
			}
			
		});
	}
}
