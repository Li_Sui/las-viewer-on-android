package main.java.nz.co.landcare.Activity;

import main.java.nz.co.landcare.Fragment.OpenFileDialogFragment;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * MainActivity
 * 
 * @author Li Sui
 *
 */
public class MainActivity extends Activity  implements OpenFileDialogFragment.OpenFileDialogListener{

	@Override
    public void onCreate( final Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        
        /**open file button*/
        Button chooseFile =( Button )findViewById ( R.id.openfile );//customer file
        Button chooseExample=( Button )findViewById(R.id.example);//example file
        
        /**
         * investigate largest memory you can use:
         * ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
         * int memoryClass = am.getMemoryClass();
         * Log.v("onCreate", "memoryClass:" + Integer.toString(memoryClass));
         * int largeMemoryClass = am.getLargeMemoryClass();
         * Log.v("onCreate", "largeMemoryClass:" + Integer.toString(largeMemoryClass));
         */
        
        /**example click listener*/
        chooseExample.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick( final View view ) {
				
			final Intent myIntent =new Intent(view.getContext(), ViewActivity.class);
			myIntent.putExtra( "fileName", "example.las" );
			myIntent.putExtra( "exampleFlag", true );
			startViewActivity( myIntent );
			}
        });
        
        /**customer file click listener*/
        chooseFile.setOnClickListener( new OnClickListener() {
        	
			@Override
			public void onClick( final View view ) {
				new OpenFileDialogFragment().show( getFragmentManager(), "dialog" );
			}
        });
    }
	
	/**customer file click dialog call back*/
	@Override
	public void onOpenFileClick( final String path, final String fileName ) {
		
		/**double check if the file end with las*/
		if(fileName.endsWith(".las")){
			final Intent myIntent =new Intent(this, ViewActivity.class);
			myIntent.putExtra( "fileName", fileName );
			myIntent.putExtra( "exampleFlag", false );
			myIntent.putExtra( "filePath", path );
			startViewActivity( myIntent );
		}else{
			Toast.makeText( this, "Can only read .las file", Toast.LENGTH_SHORT ).show();
		}
	}
	
	/**
	 * Start new activity
	 * @param myIntent
	 */
	private void startViewActivity( final Intent myIntent ){
		
		this.startActivity( myIntent );
	}
	
	/**inflate the menu*/
	@Override
	public boolean onCreateOptionsMenu( final Menu menu ) {

		getMenuInflater().inflate( R.menu.main, menu );
		return true;
	}
	
	/**
	 * resume the view
	 */
	@Override
	protected void onResume() {
		
		super.onResume();
	}

	/**
	 * pause the view
	 */
	@Override
	protected void onPause() {
		super.onPause();
	}
}
